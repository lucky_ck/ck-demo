

[后台管理](http://lucky_ck.gitee.io/ck-demo/demo01/dist)

[动画canvas](http://lucky_ck.gitee.io/ck-demo/amint)

[vue路由](http://lucky_ck.gitee.io/ck-demo/vueRouter)

[权限管理](http://lucky_ck.gitee.io/ck-demo/vueRule)

# vue 生命周期

```vue
   beforeCreate:function(){
      alert("组件实例化之前执行的函数");
   },
   created:function(){
      alert("组件实例化完毕,但页面还未显示");
   },
   beforeMount:function(){
     alert("组件挂在前,页面仍未展示,但虚拟Dom已经配置");
   },
   mounted:function(){
     alert("组件挂在后,此方法执行后,页面显示");
   },
   beforeUpdate:function(){
     alert("组件更新前,页面仍未更新,但虚拟Dom已经配置");
   },
   updated:function(){
      alert("组件更新,此方法执行后,页面显示");
   },
   beforeDestory:function(){
      alert("组件销毁前");
   },
   destoryed:function(){
      alert("组件销毁");
  }

```



#### 简易的命令行入门教程:

Git 全局设置:

```
git config --global user.name "一切都是浮云"
git config --global user.email "1049906948@qq.com"
```

创建 git 仓库:

```
mkdir page_display
cd page_display
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/lucky_ck/ck-demo.git
git push -u origin master
```

已有仓库?

```
cd existing_git_repo
git remote add origin https://gitee.com/lucky_ck/ck-demo.git
git push -u origin master
```

##  git

git pull --rebase origin master   用在合并代码的时候其作用就是在一个随机创建的分支上处理冲突，避免了直接污染原来的分区 主分区

拷贝 git clone <仓库地址>

创建分支 git branch < name >

创建并进入分支 git checkout -b < name >

切换分支 ： git checkout < name >

查看分支： git branch -a

查看状态 git status

添加文件 git add .

提交 git commit -m ' name '

拉取 git pull

推送 git push

查看分支 git branch --list

查看分支（包括远程） git branch -a

#### master :主分支

#### dev: 开发分支

##### 版本分支：建立于dev分支下面

feature-vueAdmin-V1.0.0-2020323 分支完整名称

feature: 描述当前分支类型

vueAdmin : 项目名称

v1 0 0 :版本号

2020323：建立分支日期

##### bug分支：建立于当前版本分支下面

bug-101241-20191020: bug 分支完整名称

bug ： 分支类型

01241 ： bug 的 id

20191020 : 建立分支日期